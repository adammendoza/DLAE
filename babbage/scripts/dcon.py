#!/usr/bin/python

import sys

dtype = sys.argv[1]
infile = sys.argv[2]
outfile = sys.argv[3]
start_in = int(sys.argv[4])
start_out = int(sys.argv[5])
cardinality = int(sys.argv[6])

with open(infile, 'r') as f:
    with open(outfile, 'w') as g:

        g.write('.'+dtype+'_all:\n\n')

        if (cardinality == 0):
            # No data exists; do nothing:
            g.write('ret\n\n')

        g.write('A write in columns\n')
        g.write('A write annotation Phase \033[32;1m\''+dtype+'\'\033[0m:\n')

        g.write('N031 0\n')
        for i in xrange(cardinality):
            if (i % 100 == 0):
                g.write('N030 0\n')
                g.write('A write new line\n')

            x = int(f.readline()[-2])
            g.write('\n Actual value: '+str(x)+'\n')

            for z in xrange(2):
                g.write('N'+str(1000 + start_out + z)[1:]+' ')
                for y in xrange(5):
                    if (x == (y + 5*z)):
                        g.write('5100000000')
                    else:
                        g.write('4900000000')
                g.write('\n')
            g.write('\n')

            for j in xrange(25):
                s = f.readline()
                g.write('N'+str(1000 + start_in + j)[1:]+' '+s)

            f.readline()

            g.write('jsr '+dtype+'_datum\n')

            if (i % 100 == 99):
                g.write('A write numbers as 9\n')
                g.write('A write annotation   \n')
                g.write('R030 = R030 + R000\nP\n')
                g.write('A write annotation %\n')

        g.write('A write new line\n')
        g.write('A write in columns\n')
        g.write('A write numbers as 9\n')
        g.write('A write annotation Phase '+dtype+' completed: \033[32;1m \n')
        g.write('R031 = R031 + R000\nP\n')
        g.write('A write annotation  / '+str(cardinality)+' \033[0mimages guessed correctly.\n')
        g.write('A write new line\n')
        g.write('A write in rows\n')

        g.write('ret\n\n')

