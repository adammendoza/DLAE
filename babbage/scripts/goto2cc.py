#!/usr/bin/python

# Adam P. Goucher, January 2016

'''
goto2cc.py -- Replace jmp/cjmp instructions with combinatorial cards.
'''

import sys
import re

if (len(sys.argv) != 3):
    print("Usage: python goto2cc.py infile.xcf outfile.cf")
    exit(1)

infile = sys.argv[1]
outfile = sys.argv[2]

# Match a string that looks like '.label:':
p = re.compile('\\.([^ :]+):')

# Match a string that looks like 'jmp label' or 'cjmp label':
q = re.compile('(c?jmp) ([^ :]+)\n')

labeldict = {}

i = 0
with open(infile, 'r') as f:
    for x in f:
        i += 1
        m = p.match(x)
        if m:
            labelname = m.group(1)
            labeldict[labelname] = i

i = 0
with open(infile, 'r') as f:
    with open(outfile, 'w') as g:
        for x in f:
            i += 1
            m = q.match(x)
            if m:
                op = m.group(1)
                labelname = m.group(2)
                j = labeldict[labelname]
                if (i < j):
                    g.write('CF'+('?' if (op == 'cjmp') else '+')+str(j - i)+'\n')
                else:
                    g.write('CB'+('?' if (op == 'cjmp') else '+')+str(i - j)+'\n')
            else:
                g.write(x)

print('Successfully expanded cardfile '+infile+' to '+outfile+' ('+str(i)+' lines)')
