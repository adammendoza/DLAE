#!/bin/bash
set -e

# This script generates a cardfile to perform deep learning on
# the Analytical Engine.

# Number of samples for training and testing:
training_samples=5000
testing_samples=1000

# Set this to 1 to enable debugging:
debug_samples=0

# Learning rates for the convolutional layers:
ic_learnrate='0.0005'
hc_learnrate='0.0005'

# Learning rate for the fully-connected layer:
bpl_learnrate='0.002'


# ------------------------------------------------------------------------

# Git repositories cannot contain empty directories, so we need to
# create temp here if it doesn't already exist:
mkdir -p temp

echo 'Constructing program...'

printf "        Deep Learning with the Analytical Engine:\n\n" > temp/preinclude.xcf

cat<<FOO >> temp/preinclude.xcf

jsr print_details
jsr initialise_network
N032 0

.mainloop:

        Increment epoch counter:
R032 = R032 + 1
A write in columns
A write new line
FOO

printf "A write annotation \033[33;1m**** Epoch \nP\nA write annotation  ****\033[0m\n" >> temp/preinclude.xcf

cat<<FOO >> temp/preinclude.xcf
A write new line
A write in rows

jsr debug_all
jsr train_all
jsr eval_all

jmp mainloop

A write annotation End
B
H


.initialise_network:

A write in columns
A write annotation Initialising network
jsrn _initialise_lcg
A write annotation .
jsr ic_init
A write annotation .
jsr hc_init
A write annotation .
jsr bpl_init
A write annotation done!
A write new line
A write in rows

ret


.debug_datum:

A write in rows
jsr ic_feedforward
jsr ic_debug
jsr s1_feedforward
jsr s1_debug
jsr hc_feedforward
jsr hc_debug
jsr s2_feedforward
jsr s2_debug
jsr bpl_feedforward
jsr bpl_debug
jsr s3_feedforward
jsr s3_debug
jsr ev_evaluate
jsr s3_backprop
jsr bpl_backprop
jsr bpl_debug
jsr s2_backprop
jsr s2_debug
jsr hc_backprop
jsr hc_debug
jsr s1_backprop
jsr s1_debug
jsr ic_backprop
jsr ic_debug

ret


.eval_datum:

jsr ic_feedforward
jsr s1_feedforward
jsr hc_feedforward
jsr s2_feedforward
jsr bpl_feedforward
jsr s3_feedforward

jsr ev_evaluate

ret


.train_datum:

jsr eval_datum

jsr s3_backprop
jsr bpl_backprop
jsr s2_backprop
jsr hc_backprop
jsr s1_backprop
jsr ic_backprop

ret


FOO

printf ".print_details:\n" >> temp/preinclude.xcf
printf "A write annotation Namaste, this is \033[33;1mDeep Learning with the Analytical Engine\033[0m\n" >> temp/preinclude.xcf
printf "A write annotation                      (Adam \033[32;1mP.\033[0m Goucher, January 2016)\n" >> temp/preinclude.xcf
printf "A write annotation  \n" >> temp/preinclude.xcf
printf "ret\n" >> temp/preinclude.xcf

echo 'Building and including layers...'

python scripts/lcon.py inputconv ic temp/ic.out 40 112 800 $ic_learnrate
python scripts/lcon.py sigmoid s1 temp/s1.out 112 212 800 500
python scripts/lcon.py hiddenconv hc temp/hc.out 212 408 800 $hc_learnrate
python scripts/lcon.py sigmoid s2 temp/s2.out 408 440 800 160
python scripts/lcon.py bipartite bpl temp/bpl.out 440 794 800 160 10 $bpl_learnrate
python scripts/lcon.py sigmoidoutput s3 temp/s3.out 794 796 800 10
python scripts/lcon.py evaluate ev temp/ev.out 796 798 800
cat temp/preinclude.xcf temp/ic.out temp/hc.out temp/bpl.out temp/ev.out temp/s1.out temp/s2.out temp/s3.out > temp/layered.xcf

echo 'Including libraries...'

cat temp/layered.xcf libs/sigmoid.xcf libs/matrix.xcf libs/random.xcf libs/packing.xcf > temp/predata.xcf

echo 'Loading and including data...'

python scripts/dcon.py train ../data/training_data.in temp/train.out 40 798 $training_samples
python scripts/dcon.py 'eval' ../data/test_data.in temp/eval.out 40 798 $testing_samples
python scripts/dcon.py debug ../data/validation_data.in temp/debug.out 40 798 $debug_samples
cat temp/predata.xcf temp/train.out temp/eval.out temp/debug.out > temp/precompiled.xcf

echo 'Compiling program...'

echo ''
python scripts/jsr2goto.py temp/precompiled.xcf temp/temp.xcf
python scripts/goto2cc.py temp/temp.xcf cardfiles/suite.cf
echo ''

echo 'Removing temporary files...'
rm temp/*

echo 'Running program on the Analytical Engine:'
echo ''
java -cp classes aes cardfiles/suite.cf
echo ''
echo 'Done!'
